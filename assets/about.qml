import bb.cascades 1.0

Sheet {
    id: aboutSheet
    peekEnabled: false
    content: Page {
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    leftPadding: 8
                    ImageView {
                        imageSource: "asset:///images/logos/hs_header.png"
                    }
                    Button {
                        text: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            aboutSheet.close();
                        }
                    }
                }
            }
            appearance: TitleBarAppearance.Plain
        }
        Container {
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            layout: DockLayout {}
            bottomPadding: 48
            Container {
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                leftPadding: 48
                rightPadding: 48
                Label {
                    text: qsTr("") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    textStyle.fontSize: FontSize.Large
                }
                Label {
                    text: "<html><a href=\"http://www.hockeystreams.com\">Hockeystreams.com</a></html>"
                    textStyle.color: Color.create("#c55500")
                }
            }
        }
    }
}
