// Tabbed Pane project template
import bb.cascades 1.0
import "live"
import "onDemand"

TabbedPane {
    showTabsOnActionBar: true
    Menu.definition: AppMenu{}
    Tab {
        id: tabLive
        title: qsTr("Live")
        imageSource: "asset:///images/icon_live.png"
        LiveStreams {
            id: liveStreams
        }
    }
    Tab {
        id: tabOnDemand
        title: qsTr("On-Demand")
        imageSource: "asset:///images/icon_on_demand.png"
        OnDemand {
            id: onDemand
        }
    }
    onCreationCompleted: {
        // this slot is called when declarative scene is created
        // write post creation initialization here
        console.log("TabbedPane - onCreationCompleted()")
        
        // enable layout to adapt to the device rotation
        // don't forget to enable screen rotation in bar-bescriptor.xml (Application->Orientation->Auto-orient)
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;
        loginSheet.open();
    }
    
    onActiveTabChanged: {
        if (activeTab == tabOnDemand) {
            tabOnDemand.content.tabChanged();
        }
    }
    
    attachedObjects: [
        Login {
            id: loginSheet
            
            onLoginResult: {
                if (loginSuccess) {
                    liveStreams.getLive();
                    loginSheet.close();
                }
            }
        }
    ]
}
