// Supporting functions for QML

// filter data and return only iStream compatable streams
function filterIstream(data) {
    if (typeof data !== 'undefined' && data.length > 0) {
        var iStreamData = [];
        for (var i = 0; i < data.length; i++) {
            console.log(JSON.stringify(data[i]));
           if (data[i].isiStream == 1) {
               iStreamData.push(data[i]);
               console.log("LiveStreams: adding iStream data: isiStream = " + data[i].isiStream);
           } 
        }
    }
    return iStreamData;
}

// return logo asset location for given team
function getLogoByTeam(team) {
	switch (team) {
		case "Anaheim Ducks":
			return "asset:///images/logos/teams/big/anahiem.png"
			break;
		case "Boston Bruins":
			return "asset:///images/logos/teams/big/boston.png";
			break;
		case "Buffalo Sabres":
			return "asset:///images/logos/teams/big/buffalo.png";
			break;
		case "Buffalo Sabres Part 2":
			return "asset:///images/logos/teams/big/buffalo.png";
			break;
		case "Calgary Flames":
			return "asset:///images/logos/teams/big/calgary.png";
			break;
		case "Carolina Hurricanes":
			return "asset:///images/logos/teams/big/carolina.png";
			break;
		case "Chicago Blackhawks":
			return "asset:///images/logos/teams/big/chicago.png";
			break;
		case "Colorado Avalanche":
			return "asset:///images/logos/teams/big/colorado.png";
			break;
		case "Columbus Blue Jackets":
			return "asset:///images/logos/teams/big/columbus.png";
			break;
		case "Dallas Stars":
			return "asset:///images/logos/teams/big/dallas.png";
			break;
		case "Detroit Red Wings":
			return "asset:///images/logos/teams/big/detroit.png";
			break;
		case "Edmonton Oilers":
			return "asset:///images/logos/teams/big/edmonton.png";
			break;
		case "Florida Panthers":
			return "asset:///images/logos/teams/big/florida.png";
			break;
		case "Los Angeles Kings":
			return "asset:///images/logos/teams/big/losangeles.png";
			break;
		case "Minnesota Wild":
			return "asset:///images/logos/teams/big/minnesota.png";
			break;
		case "Montreal Canadiens":
			return "asset:///images/logos/teams/big/montreal.png";
			break;
		case "Montreal Canadiens Part 2":
			return "asset:///images/logos/teams/big/montreal.png";
			break;
		case "Nashville Predators":
			return "asset:///images/logos/teams/big/nashville.png";
			break;
		case "New Jersey Devils":
			return "asset:///images/logos/teams/big/newjersey.png";
			break;
		case "New York Islanders":
			return "asset:///images/logos/teams/big/newyorki.png";
			break;
		case "New York Rangers":
			return "asset:///images/logos/teams/big/newyorkr.png";
			break;
		case "Ottawa Senators":
			return "asset:///images/logos/teams/big/ottawa.png";
			break;
		case "Philadelphia Flyers":
			return "asset:///images/logos/teams/big/philadelphia.png";
			break;
		case "Phoenix Coyotes":
			return "asset:///images/logos/teams/big/phoenix.png";
			break;
		case "Pittsburgh Penguins":
			return "asset:///images/logos/teams/big/pittsburgh.png";
			break;
		case "RDS Montreal Canadiens":
			return "asset:///images/logos/teams/big/montreal.png";
			break;
		case "San Jose Sharks":
			return "asset:///images/logos/teams/big/sanjose.png";
			break;
		case "St Louis Blues":
			return "asset:///images/logos/teams/big/stlouis.png";
			break;
		case "Tampa Bay Lightning":
			return "asset:///images/logos/teams/big/tampabay.png";
			break;
		case "Toronto Maple Leafs":
			return "asset:///images/logos/teams/big/toronto.png";
			break;
		case "Vancouver Canucks":
			return "asset:///images/logos/teams/big/vancouver.png";
			break;
		case "Washington Capitals":
			return "asset:///images/logos/teams/big/washington.png";
			break;
		case "Winnipeg Jets":
			return "asset:///images/logos/teams/big/winnipeg.png";
			break;
		default:
			return "asset:///images/logos/teams/big/hs.png";
			break;
	}
}