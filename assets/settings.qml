import bb.cascades 1.0
import hockeystreams.com 1.0

Sheet {
    id: settingsSheet
    Page {
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    leftPadding: 8
                    ImageView {
                        imageSource: "asset:///images/logos/hs_header.png"
                    }
                    Button {
                        text: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            settingsSheet.close();
                        }
                    }
                }
            }
            appearance: TitleBarAppearance.Plain
        }
        Container {
            leftPadding: 24
            rightPadding: 24
            topPadding: 24
            Label {
                text: qsTr("Preferred location") + Retranslate.onLocaleOrLanguageChanged
                textStyle {
                    base: headerDef.style
                }
            }
            DropDown {
                id: locationList
                onSelectedOptionChanged: {
                    
                }
            }
            attachedObjects: [
                TextStyleDefinition {
                    id: headerDef
                    fontSize: FontSize.Large
                },
                ComponentDefinition {
                    id: locationOptionDef
                    content: Option {}
                }
            ]
            
            onCreationCompleted: {
                getLocations();
            }
            
            function getLocations() {
                RESTClient.getLocationsResponse.connect(handleGetLocations);
                RESTClient.getLocations();
            }
            
            function handleGetLocations(success, message, data) {
                if (success) {
                    console.log("GetLocations: " + success + " : " + message);
                    for (var i=0; i<data.length; i++) {
                        var option = locationOptionDef.createObject();
                        console.log("Settings: locateion = " + data[i].location);
                        option.text = data[i].location;
                        option.value = data[i].location;
                        locationList.add(option);
                    }
                } else {
                    console.log("GetLocations: " + success + " : " + message);
                }
                RESTClient.getLocationsResponse.disconnect(handleGetLocations);

            }
        }
    }
}