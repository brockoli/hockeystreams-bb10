import bb.cascades 1.0
import bb.system 1.0

Sheet {
    id: loginSheet
    signal loginResult(bool loginSuccess);
    Page {
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    leftPadding: 8
                    ImageView {
                        imageSource: "asset:///images/logos/hs_header.png"
                    }
                }
            }
            appearance: TitleBarAppearance.Plain
        }
        Container {
            //layout: DockLayout {}

            leftPadding: 16.0
            rightPadding: 16.0
            Container {
                layout: DockLayout {}
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1.0
                }
                horizontalAlignment: HorizontalAlignment.Center
                Container {
                    id: creds
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    Container {
                        id: username
                        bottomPadding: 24
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        TextField {
                            id: tfUsername
                            hintText: qsTr("username") + Retranslate.onLanguageChanged
                            text: Utils.getSettingValue("USERNAME")
                            inputMode: TextFieldInputMode.Password
                            preferredWidth: 500.0
                            input {
                                submitKey: SubmitKey.Next
                                flags: TextInputFlag.WordSubstitutionOff | TextInputFlag.SpellCheckOff | TextInputFlag.PredictionOff | TextInputFlag.AutoCorrectionOff | TextInputFlag.AutoCapitalizationOff | TextInputFlag.AutoPeriodOff
                                masking: TextInputMasking.NotMaskedNotTogglable
                                
                                onSubmitted: {
                                    tfPassword.requestFocus();
                                }
                            }
                        }
                    }
                    Container {
                        id: password
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        TextField {
                            id: tfPassword
                            hintText: qsTr("password") + Retranslate.onLanguageChanged
                            text: Utils.getSettingValue("PASSWORD")
                            inputMode: TextFieldInputMode.Password
                            preferredWidth: 500.0
                            input {
                                submitKey: SubmitKey.Submit
                                
                                onSubmitted: {
                                    creds.login();
                                }
                            }
                        }
                    }
                    CheckBox {
                        id: stayLoggedIn
                        text: qsTr("Remember me") + Retranslate.onLanguageChanged
                        checked: Utils.getSettingValue("STORE_CREDS")
                        preferredWidth: 500.0
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    Button {
                        id: btnLogin
                        text: qsTr("Sign in") + Retranslate.onLanguageChanged
                        preferredWidth: 500.0
                        onClicked: {
                            creds.login();
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                    
                    function login() {
                        if (tfUsername.text.length > 0 && tfPassword.text.length > 0) {
                            RESTClient.login(tfUsername.text, tfPassword.text);
                        }                        
                    }
                }
            }
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1.0
                }
            }
        }
    }
    onOpened: {
        RESTClient.loginResponse.connect(handleLogin);
    }
    onClosed: {
        RESTClient.loginResponse.disconnect(handleLogin);
    }
    
    attachedObjects: [
        SystemDialog {
            id: loginFailed
            dismissAutomatically: true
            cancelButton.label: undefined
            title: qsTr("Login failed") + Retranslate.onLanguageChanged
            body: qsTr("Please verify your username and password") + Retranslate.onLanguageChanged
        }
    ]
    
    // Handle the login REST response
    // Params:  success -- Boolean representing if the response was successfull
    //			message -- String representing the message from the REST API
    function handleLogin(success, message) {
        console.log("**** Login success = " + success + " message = " + message);
        if (!success) {
            loginFailed.show();
        }
        
        // Check if we should store user credentials
        if (stayLoggedIn.checked) {
            Utils.updateSettingValue("STORE_CREDS", stayLoggedIn.checked);
            Utils.updateSettingValue("USERNAME", tfUsername.text);
            Utils.updateSettingValue("PASSWORD", tfPassword.text);
        } else {
            Utils.updateSettingValue("STORE_CREDS", stayLoggedIn.checked);
            Utils.updateSettingValue("USERNAME", "");
            Utils.updateSettingValue("PASSWORD", "");            
        }
        loginResult(success);
    }
}
