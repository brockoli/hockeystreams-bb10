import bb.cascades 1.0
import "../components"
import "../js/utils.js" as Utils

Container {
    property variant navPane
    property bool touchHandled: false
    
    background: rowBgDef.imagePaint
    layout: DockLayout {}
    preferredWidth: 768.0
    Container {
        id: rightDetails
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Center
        rightPadding: 56
        Label {
            id: period
            text: ListItemData.period
            horizontalAlignment: HorizontalAlignment.Center
            textStyle {
                base: periodTextDef.style
            }
        }
        CustomButton {
            id: btnWatch
            text: qsTr("watch")
            bgImg: "asset:///images/button_small_orange.png"
            btnTextColor: Color.White
            btnTextSize: FontSize.Medium
            preferredWidth: 200.0
            horizontalAlignment: HorizontalAlignment.Center
            
            onCreationCompleted: {
                btnWatch.touchHandled.connect(handleTouch);
                btnWatch.clicked.connect(handleButtonClicked);
            }
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            topPadding: 12
            Container {
                horizontalAlignment: HorizontalAlignment.Center
	            Label {
	                id: gameTime
	                text: ListItemData.startTime
	                textStyle {
	                    base: timeTextDef.style
	                }
	            }
            }
            Container {     
                horizontalAlignment: HorizontalAlignment.Center
	            Label {
	                id: feedType
	                text: ListItemData.feedType
	                textStyle {
	                    base: timeTextDef.style
	                }
	            }
            }
        }
    }
    Container {
        id: gameDetails
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        topPadding: 16
        bottomPadding: 24
        leftPadding: 16
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Left
        preferredWidth: 510
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                bottomPadding: 16
                Label {
                    id: awayScore
                    text: ListItemData.awayScore
                    verticalAlignment: VerticalAlignment.Center
                    textStyle {
                        base: scoreTextDef.style
                    }
                }
                FramedLogo {
                    id: awayLogo
                    source: Utils.getLogoByTeam(ListItemData.awayTeam)
                    verticalAlignment: VerticalAlignment.Center
                }
                Container {
                    id: awayTeamName
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: 16
                    Container {
                    Label {
                        id: awayCity
                        text: getCity(ListItemData.awayTeam);
                        textStyle {
                            base: cityTextDef.style
                        }
                    }}
                    Container {
                    Label {
                        id: awayTeam
                        text: getTeam(ListItemData.awayTeam);
                        textStyle {
                            base: teamTextDef.style
                        }
                    }}
                }
            }
            Container {
                layout: DockLayout {}
                bottomPadding: 12.0
                ImageView {
                    imageSource: "asset:///images/separator.amd"
                    verticalAlignment: VerticalAlignment.Center
                    preferredWidth: 460.0
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: 110.0
                    bottomPadding: 10.0
                    Label {
	                    id: vs
	                    text: "at"
	                    textStyle {
	                        base: vsTextDef.style
	                    }
	                }
	            }
            }
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Label {
                    id: homeScore
                    text: ListItemData.homeScore
                    verticalAlignment: VerticalAlignment.Center
                    textStyle {
                        base: scoreTextDef.style
                    }
                }
                FramedLogo {
                    id: homeLogo
                    source: Utils.getLogoByTeam(ListItemData.homeTeam)
                    verticalAlignment: VerticalAlignment.Center
                }
                Container {
                    id: homeTeamName
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }
                    leftPadding: 16
                    verticalAlignment: VerticalAlignment.Center
                    Container{
                    Label {
                        id: homeCity
                        text: getCity(ListItemData.homeTeam);
                        textStyle {
                            base: cityTextDef.style
                        }
                    }}
                    Container {
                    Label {
                        id: homeTeam
                        text: getTeam(ListItemData.homeTeam);
                        textStyle {
                            base: teamTextDef.style
                        }
                    }}
                }
            }            
            attachedObjects: [
                TextStyleDefinition {
                    id: scoreTextDef
                    color: Color.Black
                    fontSize: FontSize.XXLarge
                },
                TextStyleDefinition {
                    id: cityTextDef
                    color: Color.Black
                    fontSize: FontSize.Large
                },
                TextStyleDefinition {
                    id: teamTextDef
                    color: Color.Black
                    fontSize: FontSize.Medium
                },
                TextStyleDefinition {
                    id: vsTextDef
                    color: Color.Black
                    fontSize: FontSize.Small
                },
                ImagePaintDefinition {
                    id: rowBgDef
                    imageSource: "asset:///images/bg_white_box_center.png"
                }
            ]
        }
    }
    Container {
        property bool open: false
        id: options
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        preferredWidth: 738.0
        translationX: 724.0
        Container {
            layout: DockLayout {}
            verticalAlignment: VerticalAlignment.Center
            ImageView {
                imageSource: "asset:///images/button_drawer.png"
            }
            ImageView {
                id: drawerArrow
                imageSource: "asset:///images/icon_arrow.png"
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                rotationZ: 180.0
            }
            attachedObjects: [
                LayoutUpdateHandler {
                    id: handleLayoutHandler
                    onLayoutFrameChanged: {
                        optionsDrawer.preferredHeight = layoutFrame.height;
                    }
                }
            ]
        }
        Container {
            id: optionsDrawer
            background: bgDrawerDef.imagePaint
            opacity: 0.9
            verticalAlignment: VerticalAlignment.Center
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1.0
            }
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1.2
                }
                layout: DockLayout {
                }
                verticalAlignment: VerticalAlignment.Fill
                CustomToggleButton {
                    id: toggleHd
                    bgImg: "asset:///images/icon_hd_available.png"
                    bgPressedImg: "asset:///images/icon_hd_selected.png"
                    scalingMethod: ScalingMethod.AspectFit
                    
                    onCreationCompleted: {
                        toggleHd.touchHandled.connect(handleTouch);
                    }
                    verticalAlignment: VerticalAlignment.Center
                }
            }
            Container {
                background: Color.create("#b1b1b1")
                preferredWidth: 4
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Right
            }
            Container {
                property int type: 0
                signal touchHandled()
                onCreationCompleted: {
                    gameType.touchHandled.connect(handleTouch);
                }

                id: gameType
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 2.0
                }
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }
                verticalAlignment: VerticalAlignment.Center
                Container {
                    horizontalAlignment: HorizontalAlignment.Fill
                    bottomPadding: 28
                    layout: DockLayout {}
                    Label {
                        property bool isSelected: false
                        id: optionFullgame                        
                        text: qsTr("Full game")
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        textStyle {
                            base: gameOptionStyleDef.style
                        }
                        
                        onTouch: {
                            if (event.touchType == TouchType.Up) {
                                gameType.selectGameType(0);
                            }
                            gameType.touchHandled();
                        }
                    }
                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Fill
                    topPadding: 14
                    bottomPadding: 14
                    layout: DockLayout {}
                    Label {
                        property bool isSelected: false
                        id: optionCondensed
                        text: qsTr("Condensed")
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        textStyle {
                            base: gameOptionStyleDef.style
                        }
                        onTouch: {
                            if (event.touchType == TouchType.Up) {
                                gameType.selectGameType(1);
                            }
                            gameType.touchHandled();
                        }
                    }
                }
                Container {
                    horizontalAlignment: HorizontalAlignment.Fill
                    topPadding: 28
                    layout: DockLayout {}
                    Label {
                        property bool isSelected: false
                        id: optionHighlights
                        text: qsTr("Highlights")
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        textStyle {
                            base: gameOptionStyleDef.style
                        }
                        onTouch: {
                            if (event.touchType == TouchType.Up) {
                                gameType.selectGameType(2);
                            }
                            gameType.touchHandled();
                        }
                    }
                }
                function selectGameType(newType) {
                    gameType.type = newType;
                    switch (newType) {
                        case 0:
                            if (!optionFullgame.isSelected) {
                                optionFullgame.textStyle.color = Color.create("#4b31a1")
                                optionFullgame.isSelected = true;
                                optionCondensed.textStyle.color = Color.White
                                optionCondensed.isSelected = false;
                                optionHighlights.textStyle.color = Color.White
                                optionHighlights.isSelected = false;
                            }
                            break;
                        case 1:
                            if (! optionCondensed.isSelected) {
                                optionFullgame.textStyle.color = Color.White
                                optionFullgame.isSelected = false;
                                optionCondensed.textStyle.color = Color.create("#4b31a1")
                                optionCondensed.isSelected = true;
                                optionHighlights.textStyle.color = Color.White
                                optionHighlights.isSelected = false;
                            }
                            break;
                        case 2:
                            if (!optionHighlights.isSelected) {
                                optionFullgame.textStyle.color = Color.White
                                optionFullgame.isSelected = false;
                                optionCondensed.textStyle.color = Color.White
                                optionCondensed.isSelected = false;
                                optionHighlights.textStyle.color = Color.create("#4b31a1")
                                optionHighlights.isSelected = true;
                            }
                            break;
                        default:
                            optionFullgame.textStyle.color = Color.create("#4b31a1")
                            optionFullgame.isSelected = true;
                            optionCondensed.textStyle.color = Color.White
                            optionCondensed.isSelected = false;
                            optionHighlights.textStyle.color = Color.White
                            optionHighlights.isSelected = false;
                            break;
                    }
                }
            }
            Container {
                background: Color.create("#b1b1b1")
                preferredWidth: 4
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Right
            }
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 3.0
                }
                layout: DockLayout {}
                verticalAlignment: VerticalAlignment.Fill
                rightPadding: 56
                CustomButton {
                    id: btnWatchDrawer
                    text: qsTr("watch")
                    bgImg: "asset:///images/button_small_orange.png"
                    btnTextColor: Color.White
                    btnTextSize: FontSize.Medium
                    preferredWidth: 200.0
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center

                    onCreationCompleted: {
                        btnWatchDrawer.touchHandled.connect(handleTouch);
                        btnWatchDrawer.clicked.connect(handleButtonClicked);
                    }
                }
                onTouch: {
                    if (event.touchType == TouchType.Up) {
                        options.translationX = 724;
                    }
                }
            }
        }
        attachedObjects: [
            ImagePaintDefinition {
                id: bgDrawerDef
                imageSource: "asset:///images/bg_watch_drawer.amd"
            },
            TextStyleDefinition {
                id: gameOptionStyleDef
                color: Color.White
                fontSize: FontSize.Small
                fontWeight: FontWeight.W700
                textAlign: TextAlign.Center
            }
        ]
    }
    onTouch: {
        if (!touchHandled) {
            if (event.touchType == TouchType.Up) {
                toggleDrawer();
            }
        }
    }

	onTouchCapture: {
    	touchHandled = false; 
    }
	
    attachedObjects: [
        LayoutUpdateHandler {
            id: layoutHandler
            onLayoutFrameChanged: {
                options.preferredHeight = layoutFrame.height;
            }
        },
        TextStyleDefinition {
            id: periodTextDef
            color: Color.create("#555555")
            fontSize: FontSize.Medium
        },
        TextStyleDefinition {
            id: timeTextDef
            color: Color.create("#555555")
            fontSize: FontSize.XSmall
        }
    ]
    
    function toggleDrawer() {
        if (options.open) {
            drawerArrow.rotationZ = 180.0
            options.translationX = 724;
            options.open = false;
        } else {
            drawerArrow.rotationZ = 0.0
            options.translationX = 30;
            options.open = true;
        }
    }
    
    function handleButtonClicked() {
        var uiState;
        if (options.open) {
            toggleDrawer();
            var uiState = {
                "id": ListItemData.id,
                "isHd": toggleHd.toggleOn,
                "type": gameType.type
            };
            console.log("GameItem: id=" + uiState.id + " isHd=" + uiState.isHd + " type=" + uiState.type);
        } else {
            var uiState = {
                "id": ListItemData.id,
                "isHd": false,
                "type": 0
            };
            console.log("GameItem: id=" + uiState.id + " isHd=" + uiState.isHd + " type=" + uiState.type);
        }
        if (Boolean(navPane)) {
            navPane.getVideoStream(uiState);
        }
    }
    
    function handleTouch() {
        touchHandled = true;
    }
    
    function getCity(data) {
        if (typeof data === 'string') {
            var sData = data.split(" ");
            var city = "";
            for (var i=0; i < sData.length-1; i++) {
                city = city + " " + sData[i];
            }
            return city.substring(1);
        }
        return "";
    }
    function getTeam(data) {
        if (typeof data === 'string') {
            var sData = data.split(" ");
            return sData[sData.length-1];
        }
        return "";
    }
}
