import bb.cascades 1.0
import "../common"

Container {
    background: listBgImageDef.imagePaint
    Button {
        id: btnOpenTeamPicker
        enabled: false
        text: qsTr("On Demand - Pick team")
        horizontalAlignment: HorizontalAlignment.Center
        onClicked: {
            teamPicker.open();
        }
    }
    ListView {
        property variant navPane: onDemandPane
        id: onDemandList
        dataModel: onDemandDataModel
        listItemComponents: [
            ListItemComponent {
                GameItem {
                    navPane: ListItem.view.navPane
                }
            }
        ]
        onTriggered: {
/*            var videoPage = videoView.createObject();
            onDemandPane.peekEnabled = false;
            onDemandPane.push(videoPage);
            // Kick off a REST API request to get the selected on demand stream
            var selectedItem = dataModel.data(indexPath);
            videoPage.getOnDemandStream(selectedItem.id);
*/        }
    }
    attachedObjects: [
        Sheet {
            id: teamPicker
            content: Page {
                titleBar: TitleBar {
                    id: teamPickerTitle
                    title: qsTr("Pick a team")
                    kind: TitleBarKind.Default
                    dismissAction: ActionItem {
                        title: qsTr("Cancel")
                        onTriggered: {
                            teamPicker.close();
                        }
                    }
                }
                content: Container {
                    ListView {
                        id: teamsList
                        dataModel: teamsDataModel
                        listItemComponents: [
                            ListItemComponent {
                                Label {
                                    text: ListItemData.name
                                }
                            }
                        ]
                        
                        onTriggered: {
                            var selectedItem = dataModel.data(indexPath);
                            getOnDemand("", selectedItem.name);
                            //testLabel.text = selectedItem.name;
                            teamPicker.close();
                        }
                    }
                    attachedObjects: [
                        ArrayDataModel {
                            id: teamsDataModel
                        }
                    ]
                }
            }

            // Handle the listTeams REST response
            // Params:  success -- Boolean representing if the response was successfull
            //          message -- String representing the message from the REST API
            //          data    -- QVariantMap representing the JSON data returned from the REST API
            function handleListTeams(success, message, data) {
                if (success) {
                    console.log("ListTeams: " + success + " : " + message);
                    teamsDataModel.clear();
                    teamsDataModel.append(data);
                    btnOpenTeamPicker.enabled = true;
                } else {
                    console.log("ListTeams: " + success + " : " + message);
                }
                RESTClient.listTeamsResponse.disconnect(handleListTeams);
            }

            // Make the listTeams REST API call
            function listTeams() {
                RESTClient.listTeamsResponse.connect(handleListTeams);
                RESTClient.listTeams();
            }
        },
        ImagePaintDefinition {
            id: listBgImageDef
            imageSource: "asset:///images/bg_gradient_white_box.png"
        }
    ]
    function getTeams() {
        teamPicker.listTeams();
    } 
}
