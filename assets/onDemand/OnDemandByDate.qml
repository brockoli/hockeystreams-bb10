import bb.cascades 1.0
import "../common"

Container {
    Container {
        topPadding: 16
        bottomPadding: 16
        horizontalAlignment: HorizontalAlignment.Center
        DateTimePicker {
            id: dtPicker
            title: qsTr("Date")
            value: {
                new Date();
            }
            enabled: false

            onExpandedChanged: {
                if (! expanded) {
                    getOnDemand(formatDate(dtPicker.value), "");
                }
            }
        }
    }

    Container {
        background: listBgImageDef.imagePaint
        ListView {
            property variant navPane: onDemandPane
            id: onDemandList
	        dataModel: onDemandDataModel
	        listItemComponents: [
	            ListItemComponent {
	                GameItem {
	                    navPane: ListItem.view.navPane
	                }
	            }
	        ]
	        onTriggered: {
/*	            var videoPage = videoView.createObject();
	            onDemandPane.peekEnabled = false;
	            onDemandPane.push(videoPage);
	            // Kick off a REST API request to get the selected on demand stream
	            var selectedItem = dataModel.data(indexPath);
	            videoPage.getOnDemandStream(selectedItem.id);
*/	        }
	    }
    }

	attachedObjects: [
        ImagePaintDefinition {
            id: listBgImageDef
            imageSource: "asset:///images/bg_gradient_white_box.png"
        },
        TextStyleDefinition {
            id: onDemandHeaderTextDef
            fontSize: FontSize.Medium
            color: Color.White
        }
    ]

    // Handle the getOnDemandDates REST response
    // Params:  success -- Boolean representing if the response was successfull
    //			message -- String representing the message from the REST API
    //			data 	-- QVariantMap representing the JSON data returned from the REST API
    function handleGetOnDemandDates(success, message, data) {
        if (success) {
            console.log("GetOnDemandDates: " + success + " : " + message);
            var maxDate = new Date(parseYear(data[0]), parseMonth(data[0]), parseDay(data[0])).toISOString();
            var minDate = new Date(parseYear(data[data.length - 1]), parseMonth(data[data.length - 1]), parseDay(data[data.length - 1])).toISOString();
            dtPicker.setMinimum(minDate);
            dtPicker.setMaximum(maxDate);
            dtPicker.enabled = true;
        } else {
            console.log("GetOnDemandDates: " + success + " : " + message);
        }
        RESTClient.getOnDemandDatesResponse.disconnect(handleGetOnDemandDates);
    }

    // Make the getOnDemandDates REST API call
    function getOnDemandDates() {
        RESTClient.getOnDemandDatesResponse.connect(handleGetOnDemandDates);
        RESTClient.getOnDemandDates();
    }

    function formatDate(value) {
        return ("0" + (value.getMonth() + 1)).slice(-2) + "/" + ("0" + value.getDate()).slice(-2) + "/" + value.getFullYear();
    }
    
    function parseMonth(value) {
        var x = value.split("/");
        return x[0] - 1;
    }
    
    function parseDay(value) {
        var x = value.split("/");
        return x[1];
    }
    
    function parseYear(value) {
        var x = value.split("/");
        return x[2];
    }
}
