import bb.cascades 1.0
import hockeystreams.com 1.0
import "../videoPlayer"
import "../js/utils.js" as Utils

NavigationPane {
    property variant uiState: ""
    
    id: onDemandPane
    Page {
        id: onDemandStreams
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    leftPadding: 8
                    ImageView {
                        imageSource: "asset:///images/logos/hs_header.png"
                    }
                }
            }
            appearance: TitleBarAppearance.Plain
        }
        Container {
            SegmentedControl {
                id: onDemandTitleBar
                options: [
                    Option {
                        text: qsTr("Date")
                    },
                    Option {
                        text: qsTr("Team")
                    }
                ]
                onSelectedIndexChanged: {
                    switch (selectedIndex) {
                        case 0:
                            showByDate();
                            break;
                        case 1:
                            showByTeam();
                            break;
                    }
                }
            }
            Container {
                id: dateContainer
                visible: true
                OnDemandByDate {
                    id: vodByDate
                }
                onVisibleChanged: {
                    console.log("dateContainer visibility changed: " + visible);
                }
            }
            Container {
                id: teamContainer
                visible: false
                OnDemandByTeam {
                    id: vodByTeam
                }
                onVisibleChanged: {
                    console.log("teamContainer visibility changed: " + visible);
                }
            }
            attachedObjects: [
                ArrayDataModel {
                    id: onDemandDataModel
                },
                ComponentDefinition {
                    id: videoView
                    source: "../videoPlayer/VideoPage.qml"
                }
            ]
        }
    }
    
    onPopTransitionEnded: {
        page.destroy();
        peekEnabled = true;
    }

	attachedObjects: [
	    VideoPlayer{
	        id: videoPlayer
	    }
	]
    function startVideoPlayer(src, title) {
        //var videoPage = videoView.createObject();
        //onDemandPane.peekEnabled = false;
        //onDemandPane.push(videoPage);
        // Kick off a REST API request to get the selected Live stream
        //videoPage.handleVideoUrlReady(src);
        
        videoPlayer.playMedia(src, title);
    }

	// uiState.id = stream id, .isHd = bool, type 0=full, 1=condensed, 2=highlights
	function getVideoStream(data) {
	    uiState = data;
        console.log("OnDemand: id=" + uiState.id + " isHd=" + uiState.isHd + " type=" + uiState.type);
        getOnDemandStream(uiState.id);
	}
	
    function tabChanged() {
	    if (onDemandTitleBar.selectedIndex == 0) {
	        vodByDate.getOnDemandDates();
	    }
	}
	
	function showByDate() {
        teamContainer.visible = false;
        onDemandDataModel.clear();
        dateContainer.visible = true;
    }
	
	function showByTeam() {
	    vodByTeam.getTeams();
        dateContainer.visible = false;
        onDemandDataModel.clear();
        teamContainer.visible = true;
    }
	
    // Handle the getOnDemand REST response
    // Params:  success -- Boolean representing if the response was successfull
    //			message -- String representing the message from the REST API
    //			data 	-- QVariantMap representing the JSON data returned from the REST API
    function handleGetOnDemand(success, message, data) {
        if (success) {
            console.log("GetOnDemand: " + success + " : " + message);
            onDemandDataModel.clear();
            onDemandDataModel.append(Utils.filterIstream(data));
        } else {
            console.log("GetOnDemand: " + success + " : " + message);
        }
        RESTClient.getOnDemandResponse.disconnect(handleGetOnDemand);
    }

    // Handle the getOnDemandStream REST response
    // Params:  success -- Boolean representing if the response was successfull
    //          message -- String representing the message from the REST API
    //          data    -- QVariantMap representing the JSON data returned from the REST API
    function handleGetOnDemandStream(success, message, data) {
        if (success) {
            console.log("GetOnDemandStream: " + success + " : " + message);
            var streamSrc;
            console.log("OnDemand: uiState.type = " + uiState.type);
            switch (uiState.type) {
                case 0:  // full game
                    var streams = data.streams;
                    for (var i = 0; i < streams.length; i ++) {
                        if (streams[i].type == "iStream") {
                            streamSrc = streams[i].src;
                            break;
                        }
                    }
                    if (!uiState.isHd) {
                        var re = /H(?=D.f4f)/;
                        streamSrc = streamSrc.replace(re, "S");
                    }
					break;
                case 1:  // condensed
                	var condensed = data.condensed;
                	streamSrc = condensed[0].homeSrc;
                	console.log("OnDemand: streamSrc = " + streamSrc);
                    break;
                case 2:  // highlights
                	var highlights = data.highlights;
                	streamSrc = highlights[0].homeSrc;
                    console.log("OnDemand: streamSrc = " + streamSrc);
                    break;
                default:  // full game
                    break;
            }
            if (streamSrc) {
                console.log("OnDemand: streamSrc = " + streamSrc);
                startVideoPlayer(streamSrc, data.awayTeam + " at " + data.homeTeam);
            }
        } else {
            console.log("GetOnDemandStream: " + success + " : " + message);
        }
        RESTClient.getOnDemandStreamResponse.disconnect(handleGetOnDemandStream);
    }

    // Make the getOnDemand REST API call
    function getOnDemand(newDate, newTeam) {
        RESTClient.getOnDemandResponse.connect(handleGetOnDemand);
        RESTClient.getOnDemand(newDate, newTeam);
    }

    // Make the getOnDemandStream API call
    // Params: id -- id of the selected stream in the list
    function getOnDemandStream(id) {
        RESTClient.getOnDemandStreamResponse.connect(handleGetOnDemandStream);
        RESTClient.getOnDemandStream(id);
    }
}