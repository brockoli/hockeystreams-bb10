import bb.cascades 1.0
import hockeystreams.com 1.0
import "../common"
import "../videoPlayer"
import "../js/utils.js" as Utils

NavigationPane {
    property variant uiState: ""

    id: liveStreamsPane
    Page {
        id: liveStreams
        titleBar: TitleBar {
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    leftPadding: 8
                    ImageView {
                        imageSource: "asset:///images/logos/hs_header.png"
                    }
                }
            }
            appearance: TitleBarAppearance.Plain
        }
        Container {
            background: Color.Black
            horizontalAlignment: HorizontalAlignment.Fill
            Container {
                leftPadding: 32
                Label {
                    textStyle {
                        base: liveHeaderTextDef.style
                    }
                    text: qsTr("Live - Games playing today")
                }
            }
            Container {
                background: listBgImageDef.imagePaint
                ListView {
                    property variant navPane: liveStreamsPane
                    id: liveList
                    dataModel: liveDataModel
                    listItemComponents: [
                        ListItemComponent {
                            GameItem {
                                navPane: ListItem.view.navPane
                            }
                        }
                    ]
                }
            }
        }
        attachedObjects: [
            ArrayDataModel {
                id: liveDataModel
            },
            ComponentDefinition {
                id: videoView
                source: "../videoPlayer/VideoPage.qml"
            },
            ImagePaintDefinition {
                id: listBgImageDef
                imageSource: "asset:///images/bg_gradient_white_box.png"
            },
            TextStyleDefinition {
                id: liveHeaderTextDef
                fontSize: FontSize.Medium
                color: Color.White
            }
        ]
    }
    
    onPopTransitionEnded: {
        page.destroy();
        peekEnabled = true;
    }

    attachedObjects: [
        VideoPlayer {
            id: videoPlayer
        }
    ]

    function startVideoPlayer(src, title) {
        //var videoPage = videoView.createObject();
        //liveStreamsPane.peekEnabled = false;
        //liveStreamsPane.push(videoPage);
        // Kick off a REST API request to get the selected Live stream
        //videoPage.handleVideoUrlReady(src);

        videoPlayer.playMedia(src, title);
    }

    // uiState.id = stream id, .isHd = bool, type 0=full, 1=condensed, 2=highlights
    function getVideoStream(data) {
        uiState = data;
        console.log("OnDemand: id=" + uiState.id + " isHd=" + uiState.isHd + " type=" + uiState.type);
        getLiveStream(uiState.id);
    }
	
    // Handle the getLive REST response
    // Params:  success -- Boolean representing if the response was successfull
    //			message -- String representing the message from the REST API
    //			data 	-- QVariantMap representing the JSON data returned from the REST API
    function handleGetLive(success, message, data) {
        if (success) {
            console.log("GetLive: " + success + " : " + message);
            liveDataModel.clear();
            liveDataModel.append(Utils.filterIstream(data));
        } else {
            console.log("GetLive: " + success + " : " + message);
        }
        RESTClient.getLiveResponse.disconnect(handleGetLive);
    }

    // Handle the getLiveStream REST response
    // Params:  success -- Boolean representing if the response was successfull
    //			message -- String representing the message from the REST API
    //			data 	-- QVariantMap representing the JSON data returned from the REST API
    function handleGetLiveStream(success, message, data) {
        if (success) {
            console.log("GetLiveStream: " + success + " : " + message);
            var streamSrc;
            console.log("LiveStreams: uiState.type = " + uiState.type);
            switch (uiState.type) {
                case 0: // full game
                    var streams = data.nonDVRSD;
                    for (var i = 0; i < streams.length; i ++) {
                        console.log("LiveStreams: stream.type = " + streams[i].type + " stream.location = " + streams[i].location);
                        if (streams[i].type == "iStream") {
                            console.log("LiveStreams: found valid stream");
                            streamSrc = streams[i].src;
                            break;
                        }
                    }
                    if (! uiState.isHd) {
                        var re = /H(?=D.f4f)/;
                        streamSrc = streamSrc.replace(re, "S");
                    }
                    break;
                default: // full game
                    break;
            }
            if (streamSrc) {
                console.log("LiveStreams: streamSrc = " + streamSrc);
                startVideoPlayer(streamSrc, data.awayTeam + " at " + data.homeTeam);
            }
        } else {
            console.log("GetOnDemandStream: " + success + " : " + message);
        }
        RESTClient.getLiveStreamResponse.disconnect(handleGetOnDemandStream);
    }

    // Make the getLive REST API call
    function getLive() {
        RESTClient.getLiveResponse.connect(handleGetLive);
        RESTClient.getLive();
    }
    
    // Make the getLiveStream API call
    // Params: id -- id of the selected stream in the list
    function getLiveStream(id) {
        RESTClient.getLiveStreamResponse.connect(handleGetLiveStream);
        RESTClient.getLiveStream(id);
    }
}