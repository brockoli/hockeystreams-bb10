import bb.cascades 1.0

Container {
    property alias text: btnLabel.text
    property alias bgImg: btnBg.imageSource
    property alias bgPressedImg: btnPressedBg.imageSource
    property alias btnTextColor: btnTextStyle.color
    property alias btnTextSize: btnTextStyle.fontSize
    
    property int btnX: 0
    property int btnY: 0
    property int btnWidth: 0
    property int btnHeight: 0
    
    signal clicked();
    signal touchHandled();
    
    id: customButton
    background: btnBg.imagePaint
    topPadding: 16.0
    bottomPadding: 26.0
    Label {
        id: btnLabel
        text: "Testing"
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Fill
        textStyle { base: btnTextStyle.style }
    }
    
    onTouch: {
        if (event.isDown()) {
            customButton.background = btnPressedBg.imagePaint;
        } else if (event.isUp()) {
            if (((event.localX + btnX) > btnX && (event.localX + btnX) < (btnX + btnWidth)) && ((event.localY + btnY) > btnY && (event.localY + btnY) < (btnY + btnHeight))) {
                clicked();
            }
            customButton.background = btnBg.imagePaint;
        } else if (event.isMove()) {
            if (((event.localX + btnX) > btnX && (event.localX + btnX) < (btnX + btnWidth))
                && ((event.localY + btnY) > btnY && (event.localY + btnY) < (btnY + btnHeight))) {
                customButton.background = btnPressedBg.imagePaint;
            } else {
                customButton.background = btnBg.imagePaint;
            }
        }
        touchHandled();
    }
    
    touchBehaviors: [
        TouchBehavior {
            TouchReaction {
                eventType: TouchType.Move
                phase: PropagationPhase.Capturing
                response: TouchResponse.StartTracking
            }
        }
    ]
    attachedObjects: [
        ImagePaintDefinition {
            id: btnBg
        },
        ImagePaintDefinition {
            id: btnPressedBg
        },
        TextStyleDefinition {
            id: btnTextStyle
            fontSize: FontSize.Small
            textAlign: TextAlign.Center       
        },
        LayoutUpdateHandler {
            id: btnLayoutHandler
            onLayoutFrameChanged: {
                btnX = layoutFrame.x;
                btnY = layoutFrame.y;
                btnWidth = layoutFrame.width;
                btnHeight = layoutFrame.height;
            }
        }
    ]
}