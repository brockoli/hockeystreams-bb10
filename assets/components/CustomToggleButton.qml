import bb.cascades 1.0

Container {
    property string bgImg: ""
    property string bgPressedImg: ""
    property alias scalingMethod: btnImg.scalingMethod
    
    property int btnX: 0
    property int btnY: 0
    property int btnWidth: 0
    property int btnHeight: 0
    property bool toggleOn: false
    
    signal clicked();
    signal touchHandled();
    
    id: customButton
    ImageView {
        id: btnImg
        imageSource: bgImg
        scalingMethod: ScalingMethod.AspectFit
    }
    
    onTouch: {
        if (event.isDown()) {
            if (toggleOn) {
                btnImg.imageSource = bgImg;
                toggleOn = false;
            } else {
                btnImg.imageSource = bgPressedImg;
                toggleOn = true;
            }
        }
        clicked(toggleOn);
        touchHandled();
    }
    
    touchBehaviors: [
        TouchBehavior {
            TouchReaction {
                eventType: TouchType.Move
                phase: PropagationPhase.Capturing
                response: TouchResponse.StartTracking
            }
        }
    ]
    attachedObjects: [
        ImagePaintDefinition {
            id: btnBg
        },
        ImagePaintDefinition {
            id: btnPressedBg
        },
        LayoutUpdateHandler {
            id: btnLayoutHandler
            onLayoutFrameChanged: {
                btnX = layoutFrame.x;
                btnY = layoutFrame.y;
                btnWidth = layoutFrame.width;
                btnHeight = layoutFrame.height;
            }
        }
    ]
}