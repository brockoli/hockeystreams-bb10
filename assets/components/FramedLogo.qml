import bb.cascades 1.0

Container {
    property alias source: logo.imageSource
    
    layout: AbsoluteLayout {
    }
    verticalAlignment: VerticalAlignment.Center
    ImageView {
        id: logo
        preferredWidth: 140.0
        preferredHeight: 90.0
    }
    ImageView {
        imageSource: "asset:///images/logos/team_overlay.png"
        preferredWidth: 140.0
        preferredHeight: 92.0
    }
}
