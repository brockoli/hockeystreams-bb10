import bb.cascades 1.0
import bb.multimedia 1.0
import hockeystreams.com 1.0

Page {
    id: videoScreen
    property variant videoData:null
    property int  portraitModePlayerHeight: 432
    property int  landscapeModePlayerHeight: 720
    property int fullScreenWidth: 768
    property int fullScreenHeight: 1280
    property int panelPortraitPadding: 140
    property int panelLandscapePadding: 100
    actionBarVisibility:ChromeVisibility.Overlay

    Container {
        background: Color.Black
        layout: DockLayout {
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            overlapTouchPolicy: OverlapTouchPolicy.Allow
            layout: DockLayout {
            }
            Container {
                id: playerContianer
                preferredHeight: videoScreen.portraitModePlayerHeight
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Center
                layout: DockLayout {}
                ForeignWindowControl {
                    id: videoSurface
                    windowId: "VideoSurface"
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    visible: boundToWindow
                    updatedProperties: WindowProperty.Size | WindowProperty.Position | WindowProperty.Visible
                }
                ActivityIndicator {
                    id: videoLoading
                    preferredWidth: 200.0
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                }
            }
            Container {
                id: bottomPanel
                bottomPadding : videoScreen.panelPortraitPadding //since using overlay action bar
                preferredHeight: 140
                preferredWidth: videoScreen.fullScreenWidth;
                verticalAlignment: VerticalAlignment.Bottom
                overlapTouchPolicy: OverlapTouchPolicy.Allow
                layout: DockLayout {
                }
                Container {
                    //Used as background for the entire control panel
                    preferredHeight: bottomPanel.preferredHeight
                    preferredWidth: bottomPanel.preferredWidth
                    background: Color.Black
                    opacity: 0.5
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Left
                    overlapTouchPolicy: OverlapTouchPolicy.Allow
                    leftPadding: 10
                    Label {
                        id: currentTime
                        horizontalAlignment: HorizontalAlignment.Fill
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.XSmall
                        }
                        text: "00:00"
                    }
                }
                Container {
                    id: sliderContainer
                    verticalAlignment: VerticalAlignment.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    overlapTouchPolicy: OverlapTouchPolicy.Allow
                    Slider {
                        id: durationSlider
                        preferredWidth: bottomPanel.preferredWidth - 220
                        onValueChanged: {
                            if (! player.sliderValueChange) {
                                player.seekTime(durationSlider.value);
                            }
                        }
                        onImmediateValueChanged: {
                            // Don't hide the control while the user is seeking
                            if (! player.sliderValueChange) {
                                hidePanelTimer.start();
                            }
                        }
                    }
                }
                Container { 
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    rightPadding: 10
                    Label {
                        id: durationLabel
                        textStyle {
                            color: Color.White
                            fontSize: FontSize.XSmall
                        }
                        text: "00:00"
                    }
                }
                animations: [
                    FadeTransition {
                        id: panelDisappearOpacityAnimation
                        duration: 500
                        easingCurve: StockCurve.CubicOut
                        toOpacity: 0.0
                    },
                    FadeTransition {
                        id: panelAppearOpacityAnimation
                        duration: 500
                        easingCurve: StockCurve.CubicOut
                        toOpacity: 1.0
                    }
                ]
            }
        }
        onTouch: {
            showControls();
        }
    }
    actions: [
        ActionItem {
            id:playButton
            imageSource: "asset:///images/play.png"
            title: "Play"
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                hidePanelTimer.start();
                if(player.mediaState == MediaState.Started) {
                    videoScreen.pauseMediaPlayer();  
                }
                else {
                    videoScreen.playMediaPlayer() === MediaError.None
                }
            }
        }
    ]
    attachedObjects: [
        MediaPlayer {
            id: player
            property bool sliderValueChange:false
            sourceUrl: ""
            windowId: videoSurface.windowId 
            videoOutput: VideoOutput.PrimaryDisplay
            onDurationChanged: {
                durationSlider.toValue = duration;
                durationLabel.text = formatTime(duration);
            }
            onPositionChanged: {
                if(player.mediaState == MediaState.Started) {
                    player.sliderValueChange = true;
                    durationSlider.setValue(player.position)
                    player.sliderValueChange = false
                    currentTime.text = formatTime(player.position);
                }
            }
            onBufferStatusChanged: {
                if (player.bufferStatus == BufferStatus.Playing) {
                    if (videoLoading.running) {
                        videoLoading.stop();
                    }
                } else if (player.bufferStatus == BufferStatus.Buffering) {
                    if (!videoLoading.running) {
                        videoLoading.start();
                    }
                }
            }
            onError: {
                console.log("Hockeystreams MediaPlayer: ERROR at position (" + position + "): " + mediaError);
            }
            onBuffering: {
                console.log("Hockeystreams MediaPlayer: BUFFERING " + percentage*100 + "%");
            }
        }, 
        QTimer {
            id: hidePanelTimer
            interval: 2000 //2 seconds to hide
            onTimeout: {
                hideControls();
            }
        },
        OrientationHandler {
            onOrientationAboutToChange: {
                if (orientation == UIOrientation.Landscape) {
                    playerContianer.preferredHeight = videoScreen.landscapeModePlayerHeight;
                    bottomPanel.preferredWidth = videoScreen.fullScreenHeight;
                    bottomPanel.bottomPadding = videoScreen.panelLandscapePadding;
                    //playerContianer.verticalAlignment = VerticalAlignment.Fill
                } else {
                    playerContianer.preferredHeight = videoScreen.portraitModePlayerHeight;
                    bottomPanel.preferredWidth = videoScreen.fullScreenWidth;
                    bottomPanel.bottomPadding = videoScreen.panelPortraitPadding;
                }
            }
        },
        NowPlayingConnection {
            id: nowPlaying
            connectionName: "Hockeystreams"
            overlayStyle: OverlayStyle.Fancy
            previousEnabled: false
            nextEnabled: false
            onAcquired: {
                var metadata = { "title" : "Hockeystreams" };
                
                nowPlaying.mediaState = MediaState.Started;
                nowPlaying.setMetaData(metadata);
            }
            
            onPause: {
                playButton.imageSource = "asset:///images/play.png";
                playButton.title = "Play"
                player.pause();
            }
            
            onPlay: {
                playButton.imageSource = "asset:///images/pause.png";
                playButton.title = "Pause"
                player.play();
            }
        }
    ]
    onCreationCompleted: {
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;

        videoLoading.start();
        hidePanelTimer.start();
    }
    function handleVideoUrlReady(url) {
        console.log("handleVideoUrlReady: " + url);
        player.sourceUrl = url;
        playMediaPlayer();
    }

    function playMediaPlayer() {
        playButton.imageSource = "asset:///images/pause.png";
        playButton.title = "Pause"
        nowPlaying.acquire();
        nowPlaying.mediaState = MediaState.Started;
        return player.play();
    }

    function pauseMediaPlayer() {
        playButton.imageSource = "asset:///images/play.png";
        playButton.title = "Play"
        nowPlaying.mediaState = MediaState.Paused;
        return player.pause();
    }
    function formatTime(msecs)
    {
            var seconds = msecs/1000;
            function format(number) {
                var t = Math.floor(number);
                if (t < 10)
                  t = "0" + t;
                return t;
            }

            var mins = format(seconds / 60);
            var reminder = seconds - mins * 60;
            var secs = format(reminder);

            return mins + ":" + secs;
    }

    function hideControls() {
        videoScreen.actionBarVisibility = ChromeVisibility.Hidden;
        panelDisappearOpacityAnimation.play();
        hidePanelTimer.stop();
    }

    function showControls() {
        videoScreen.actionBarVisibility = ChromeVisibility.Overlay;
        panelAppearOpacityAnimation.play();
        hidePanelTimer.start();
    }
}