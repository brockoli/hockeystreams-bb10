import bb.cascades 1.0

MenuDefinition {
    attachedObjects: [
        ComponentDefinition {
            id: aboutComponent
            source: "about.qml"
        },
        ComponentDefinition {
            id: settingsComponent
            source: "settings.qml"
        }
    ]
    actions: [
        ActionItem {
            title: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/ic_info.png"
            onTriggered: {
                var sheet = aboutComponent.createObject();
                sheet.open();
            }
        },
        ActionItem {
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                var sheet = settingsComponent.createObject();
                sheet.open();
            }
        }
    ]
}