APP_NAME = hockeystreams_bb10

CONFIG += qt warn_on cascades10
LIBS += -lbbdata -lbb -lbbmultimedia -lscreen -lbbsystem
QT += core

include(config.pri)
