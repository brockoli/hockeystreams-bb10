QSLOGPATH = $$PWD/src/QsLog
INCLUDEPATH += $$QSLOGPATH
#DEFINES += QS_LOG_LINE_NUMBERS
SOURCES += $$QSLOGPATH/QsLogDest.cpp \
    $$QSLOGPATH/QsLog.cpp \
    $$QSLOGPATH/QsDebugOutput.cpp

HEADERS += $$QSLOGPATH/QSLogDest.h \
    $$QSLOGPATH/QsLog.h \
    $$QSLOGPATH/QsDebugOutput.h \
    $$QSLOGPATH/QsLogLevel.h
