// Tabbed pane project template
#include "applicationui.hpp"
#include "RESTClient.hpp"
#include "VideoPlayer.hpp"
#include "Utilities.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <QTimer>

using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app)
: QObject(app)
{
    qmlRegisterType<QTimer>("hockeystreams.com", 1, 0, "QTimer");
    qmlRegisterType<VideoPlayer>("hockeystreams.com", 1, 0, "VideoPlayer");

	// create scene document from main.qml asset
    // set parent to created document to ensure it exists for the whole application lifetime
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);

    qml->setContextProperty("RESTClient", &RESTClient::getInstance());
    qml->setContextProperty("Utils", &Utilities::getInstance());


    // create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    // set created root object as a scene
    app->setScene(root);
}
