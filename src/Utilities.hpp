#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <QObject>
#include <QtCore/QVariant>

/*
 * Utilities class contains various helper methods
 * */
class Utilities : public QObject
{
	Q_OBJECT

public:
	~Utilities();

	// returns object of this singleton class
	static Utilities& getInstance();

	// Reads information from application settings file (data/settings.conf)
	Q_INVOKABLE QVariant getSettingValue(const QString &strSettingName);

	// Updates information in application settings file (data/settings.conf) based
	// on given setting name and setting value parameters
	Q_INVOKABLE void updateSettingValue(const QString &strSettingName, const QVariant &value);

private:
	Utilities();
};

#endif /* UTILITIES_HPP */
