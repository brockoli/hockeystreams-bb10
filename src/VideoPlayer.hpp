#ifndef VIDEOPLAYER_HPP_
#define VIDEOPLAYER_HPP_

#include <QObject>

class VideoPlayer : public QObject
{
	Q_OBJECT

public:
	VideoPlayer();
    Q_INVOKABLE void playMedia(const QString& url, QString title);
};

#endif /* RESTCLIENT_HPP_ */
