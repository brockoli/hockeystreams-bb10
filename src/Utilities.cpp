#include "Utilities.hpp"
#include "QsLog/QsLog.h"

#include <QSettings>

#define TAG "Utilities: "

static const QString ORGANIZATION = "HOCKEYSTREAMS.COM";
static const QString APPLICATION_NAME = "HOCKEYSTREAMS.COM.BB10";

// Utilities class singleton

Utilities::Utilities()
{
	QLOG_INFO() << TAG << "constructor";
}

Utilities::~Utilities()
{
	QLOG_INFO() << TAG << "destructor";
}

/*
 * Creates (if needed)  and returns instance of this singleton class
 * */
Utilities& Utilities::getInstance()
{
	static Utilities instance;
	return instance;
}

/*
 * Returns given setting in settings file
 * */
QVariant Utilities::getSettingValue(const QString &strSettingName)
{
	QLOG_DEBUG() << TAG << "getSettingValue: " << strSettingName;
	QSettings registrationSettings(ORGANIZATION, APPLICATION_NAME);
	return registrationSettings.value(strSettingName, "");
}

/*
 * Sets given setting to given value in settings file
 * */
void Utilities::updateSettingValue(const QString &strSettingName, const QVariant &value)
{
	QLOG_DEBUG() << TAG << "updateSettingValue: " << strSettingName;
	QSettings settings(ORGANIZATION, APPLICATION_NAME);
	settings.setValue(strSettingName, value);
	settings.sync();
}
