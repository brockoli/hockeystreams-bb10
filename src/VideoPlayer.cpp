#include "VideoPlayer.hpp"
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/PpsObject>

#include "QsLog/QsLog.h"

#define TAG "VideoPlayer: "

using namespace bb::system;
using namespace bb;

/* Constructor */
VideoPlayer::VideoPlayer()
{
	QLOG_INFO() << TAG << "created!";
}

void VideoPlayer::playMedia(const QString& url, QString title)
{
	InvokeRequest cardRequest;
	cardRequest.setTarget("sys.mediaplayer.previewer");
	QUrl qUrl = QUrl(url, QUrl::TolerantMode);
	QLOG_DEBUG() << TAG << "Title=" + title + " url=" + qUrl.toString();
	cardRequest.setUri(qUrl);
	QVariantMap map;
	map.insert("contentTitle", title);
	QByteArray requestData = PpsObject::encode(map, NULL);
	cardRequest.setData(requestData);
	InvokeManager invokeManager;
	invokeManager.invoke(cardRequest);
}
