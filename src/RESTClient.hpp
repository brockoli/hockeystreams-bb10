/*
 * RESTClient.hpp
 *
 */

#ifndef RESTCLIENT_HPP_
#define RESTCLIENT_HPP_

#include <QObject>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QtCore/QFile>
#include <QUrl>
#include <QMap>
#include <QString>

typedef QPair<QString, QString> QueryPair;
typedef QList<QueryPair> QueryList;

class RESTClient : public QObject
{
	Q_OBJECT

public:
	static const QString BASE_URL;

	static const QString API_KEY;

	virtual ~RESTClient();

	// Returns class instance
	static RESTClient& getInstance();

	Q_INVOKABLE void login(const QString& username, const QString& password);
	Q_INVOKABLE void getLive();
	Q_INVOKABLE void getLiveStream(const QString& id);
	Q_INVOKABLE void getOnDemand(const QString& date = "", const QString& team = "");
	Q_INVOKABLE void getOnDemandStream(const QString& id);
	Q_INVOKABLE void getOnDemandDates();
	Q_INVOKABLE void listTeams(const QString& league = "");
	Q_INVOKABLE void getLocations();

Q_SIGNALS:
	void loginResponse(bool success, QString message);
	void getLiveResponse(bool success, QString message, QVariant data);
	void getLiveStreamResponse(bool success, QString message, QVariant data);
	void getOnDemandResponse(bool success, QString message, QVariant data);
	void getOnDemandStreamResponse(bool success, QString message, QVariant data);
	void getOnDemandDatesResponse(bool success, QString message, QVariant data);
	void listTeamsResponse(bool success, QString message, QVariant data);
	void getLocationsResponse(bool success, QString message, QVariant data);

private:
	QNetworkAccessManager *mNetworkManager;

	QString mToken;

private:
	RESTClient();

	void post(const QString& cmd, const QueryList query);
	void get(const QString& cmd, const QueryList query);

	// Get the REST command from the request url
	QString getCmd(const QUrl& url);

private Q_SLOTS:
	void slotRequestFinished(QNetworkReply *reply);

	void slotInformativeError(QNetworkReply::NetworkError error);
};

#endif /* RESTCLIENT_HPP_ */
