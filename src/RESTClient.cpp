#include "RESTClient.hpp"

#include "QsLog/QsLog.h"

#include <bb/data/JsonDataAccess>

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QByteArray>
#include <QThread>
#include <QDir>

#define TAG "RESTClient: "

// Login Command
#define CMD_LOGIN "Login"
// Login Param keys
#define PARAM_USERNAME "username"
#define PARAM_PASSWORD "password"
#define PARAM_KEY "key"
// Login JSON keys
#define JSON_KEY_STATUS "status"
#define JSON_KEY_UID "uid"
#define JSON_KEY_USERNAME "username"
#define JSON_KEY_FAVTEAM "favteam"
#define JSON_KEY_MEMBERSHIP "membership"
#define JSON_KEY_TOKEN "token"
#define JSON_KEY_MSG "msg"
// Login response messages
#define RESPONSE_INVALID_USERNAME "Invalid username"
#define RESPONSE_INVALID_PASSWORD "Invalid password"

// GetLive Command
#define CMD_GETLIVE "GetLive"
// GetLive Param keys
#define PARAM_TOKEN "token"
// GetLive JSON keys
#define JSON_KEY_SCHEDULE "schedule"

// GetLiveStream Command
#define CMD_GETLIVESTREAM "GetLiveStream"
// GetLiveStream Param keys
#define PARAM_ID "id"
// GetLiveStream JSON keys

// GetOnDemand Command
#define CMD_GETONDEMAND "GetOnDemand"
// GetOnDemand Param keys
#define PARAM_DATE "date"
#define PARAM_TEAM "team"
//GetOnDemand JSON keys
#define JSON_KEY_ONDEMAND "ondemand"

// GetOnDemandStream Command
#define CMD_GETONDEMANDSTREAM "GetOnDemandStream"
// GetOnDemandStream Param keys
// Same as GetOnLiveStream

// GetOnDemandDates Command
#define CMD_GETONDEMANDDATES "GetOnDemandDates"
// GetOnDemandDates Param keys
#define JSON_KEY_DATES "dates"

// ListTeams Command
#define CMD_LISTTEAMS "ListTeams"
// ListTeams Param keys
#define PARAM_LEAGUE "league"
// ListTeams JSON keys
#define JSON_KEY_TEAMS "teams"

// GetLocations Command
#define CMD_GETLOCATIONS "GetLocations"
// GetLocations JSON keys
#define JSON_KEY_LOCATION "location"

using namespace bb::data;

/**
 * Qt conflates HTTP errors (layer 5) with network (layer 4 and below,
 * TCP/IP) errors.  We need to handle these separately from HTTP
 * errors.  Thankfully, the QNetworkReply::NetworkError enumeration
 * has "conveniently" placed all the HTTP errors above ordinal value
 * 100.
 */
int QNETWORKREPLY_ERROR_LAYER_5_THRESHOLD = 100;

const QString RESTClient::BASE_URL = "https://api.hockeystreams.com/";

const QString RESTClient::API_KEY = "9d0774a479892069b950b05c443382ea";

/* Constructor */
RESTClient::RESTClient()
	: mNetworkManager(0)
{
	QLOG_INFO() << TAG << "created!";
}

/* Destructor */
RESTClient::~RESTClient()
{
	QLOG_INFO() << "Deleting rest client instance";
	delete mNetworkManager;
}

void RESTClient::login(const QString& username, const QString& password)
{
	if (username.isEmpty()) {
		emit loginResponse(false, RESPONSE_INVALID_USERNAME);
		return;
	} else if (password.isEmpty()) {
		emit loginResponse(false, RESPONSE_INVALID_PASSWORD);
		return;
	} else {
		QueryList query;
		query.append(QueryPair(PARAM_USERNAME, username));
		query.append(QueryPair(PARAM_PASSWORD, password));
		query.append(QueryPair(PARAM_KEY, API_KEY));
		post(CMD_LOGIN, query);
	}
}

void RESTClient::getLive()
{
	if (!mToken.isEmpty()) {
		QueryList query;
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_GETLIVE, query);
	}
}

void RESTClient::getLiveStream(const QString& id)
{
	if (!mToken.isEmpty()) {
		QueryList query;
		query.append(QueryPair(PARAM_ID, id));
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_GETLIVESTREAM, query);
	}
}

void RESTClient::getOnDemandDates()
{
	if (!mToken.isEmpty()) {
		QueryList query;
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_GETONDEMANDDATES, query);
	}
}

void RESTClient::listTeams(const QString& league)
{
	if (!mToken.isEmpty()) {
		QueryList query;
		if (!league.isEmpty()) {
			query.append(QueryPair(PARAM_LEAGUE, league));
		}
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_LISTTEAMS, query);
	}
}

void RESTClient::getLocations()
{
	QueryList query;
	get(CMD_GETLOCATIONS, query);
}

void RESTClient::getOnDemand(const QString& date, const QString& team)
{
	if (!mToken.isEmpty()) {
		QueryList query;
		if (!date.isEmpty()) {
			query.append(QueryPair(PARAM_DATE, date));
		}
		if (!team.isEmpty()) {
			query.append(QueryPair(PARAM_TEAM, team));
		}
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_GETONDEMAND, query);
	}
}

void RESTClient::getOnDemandStream(const QString& id)
{
	if (!mToken.isEmpty()) {
		QueryList query;
		query.append(QueryPair(PARAM_ID, id));
		query.append(QueryPair(PARAM_TOKEN, mToken));
		get(CMD_GETONDEMANDSTREAM, query);
	}
}

void RESTClient::post(const QString& cmd, const QueryList query)
{
	if (mNetworkManager == 0) {
		// Create it once
		mNetworkManager = new QNetworkAccessManager(this);
		Q_ASSERT(mNetworkManager != NULL);
		bool connectResult = QObject::connect(mNetworkManager, SIGNAL(finished(QNetworkReply *)),
				SLOT(slotRequestFinished(QNetworkReply *)), Qt::DirectConnection);
		Q_ASSERT(connectResult);
	}

	QNetworkRequest request(QUrl(BASE_URL + cmd));
	request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

    QByteArray data;
    QUrl params;

    params.setQueryItems(query);
    data = params.encodedQuery();

	mNetworkManager->post(request, data);

	QLOG_DEBUG() << TAG << "post(): Url = " + request.url().toString();
	QLOG_DEBUG() << TAG << "post(): params = " + params.toString();
	QLOG_INFO() << TAG << "post() has successfully kicked off a request, on thread:" << QThread::currentThreadId();
}

void RESTClient::get(const QString& cmd, const QueryList query)
{
	if (mNetworkManager == 0) {
		// Create it once
		mNetworkManager = new QNetworkAccessManager(this);
		Q_ASSERT(mNetworkManager != NULL);
		bool connectResult = QObject::connect(mNetworkManager, SIGNAL(finished(QNetworkReply *)),
				SLOT(slotRequestFinished(QNetworkReply *)), Qt::DirectConnection);
		Q_ASSERT(connectResult);
	}

	QUrl url(BASE_URL + cmd);
	if (!query.isEmpty()) {
		url.setQueryItems(query);
	}
	QNetworkRequest request(url);
	//request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

	mNetworkManager->get(request);

	QLOG_DEBUG() << TAG << "get(): Url = " + request.url().toString();
	QLOG_INFO() << TAG << "get() has successfully kicked off a request, on thread:" << QThread::currentThreadId();
}

/*
 * As this is single tone class, this function creates (if needed)
 * and returns class intance
 * */
RESTClient& RESTClient::getInstance()
{
    static RESTClient client;
    return client;
}

void RESTClient::slotRequestFinished(QNetworkReply* reply) {
	QLOG_INFO() << "FETCH FINISHED CALLBACK";

	QVariant statusCodeV = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
	if (reply->error() == QNetworkReply::NoError)
	{
		QLOG_INFO() << "fetch finished successfully.";

		QNetworkRequest request = reply->request();
		QUrl url = request.url();
		QLOG_DEBUG() << TAG << "REQUEST URL: " << url.toString();

		QString cmd = getCmd(url);

		QByteArray bytes = reply->readAll();
		QLOG_DEBUG() << TAG << "READ ALL BYTES SUCCESSFULLY";

		QLOG_DEBUG() << TAG << "Command = " + cmd;

		// Login
		if (cmd.compare(CMD_LOGIN) == 0) {  // Handle login response
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				mToken = map.value(JSON_KEY_TOKEN).toString();
				emit loginResponse(true, "Login successfull! token = " + mToken);
			} else {
				mToken.clear();
				emit loginResponse(false, map.value(JSON_KEY_MSG).toString());
			}
		} else if (cmd.compare(CMD_GETLIVE) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				QVariantList data = map.value(JSON_KEY_SCHEDULE).toList();
				emit getLiveResponse(true, "GetLive successfull!", data);
			} else {
				emit getLiveResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_GETLIVESTREAM) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				emit getLiveStreamResponse(true, "GetLiveStream successfull!", map);
			} else {
				emit getLiveStreamResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_GETONDEMAND) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				QVariantList data = map.value(JSON_KEY_ONDEMAND).toList();
				emit getOnDemandResponse(true, "GetOnDemand successfull!", data);
			} else {
				emit getOnDemandResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_GETONDEMANDSTREAM) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				emit getOnDemandStreamResponse(true, "GetOnDemandStream successfull!", map);
			} else {
				emit getOnDemandStreamResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_GETONDEMANDDATES) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				QVariantList data = map.value(JSON_KEY_DATES).toList();
				emit getOnDemandDatesResponse(true, "GetOnDemandDates successfull!", data);
			} else {
				emit getOnDemandDatesResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_LISTTEAMS) == 0) {
			JsonDataAccess jda;
			QVariantMap map = jda.loadFromBuffer(bytes).toMap();
			if (map.value(JSON_KEY_STATUS).toString().compare("Success") == 0) {
				QVariantList data = map.value(JSON_KEY_TEAMS).toList();
				emit listTeamsResponse(true, "ListTeams successfull!", data);
			} else {
				emit listTeamsResponse(false, map.value(JSON_KEY_MSG).toString(), 0);
			}
		} else if (cmd.compare(CMD_GETLOCATIONS) == 0) { // bug in API doesn't correct format, assume success. :(
			JsonDataAccess jda;
			QVariantList list = jda.loadFromBuffer(bytes).toList();
			emit getLocationsResponse(true, "GetLocation successfull!", list);
		}
	} else {
		if(reply->error() < QNETWORKREPLY_ERROR_LAYER_5_THRESHOLD) {
			// TCP, IP, or otherwise network layer faults
			QLOG_ERROR() << "fetch request failed, network error: ";
		} else {
			// HTTP faults
			QLOG_ERROR() << "fetch request failed, HTTP error: '" + statusCodeV.toString() + "' occurred";
			if (statusCodeV.toInt() == 400) {
				// HTTP login failure code
				emit loginResponse(false, tr("Login failed"));
			}
		}
		// we don't emit the signal.
	}
}

QString RESTClient::getCmd(const QUrl& url)
{
	QString cmd = url.path();
	// Strip leading '/'
	cmd.remove(QRegExp("/"));
	return cmd;
}

void RESTClient::slotInformativeError(QNetworkReply::NetworkError error) {
	QLOG_ERROR() << "A request encountered an error";
}
